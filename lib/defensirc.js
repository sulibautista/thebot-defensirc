"use strict";

const
  _ = require('lodash'),
  util = require('util'),
  Client = require('./irc').Client;

module.exports = DefensIRC;

function DefensIRC(opts){
  opts.debug = true;
  opts.password = opts.pass;
  opts.port = 80;
  Client.call(this, 'irc.twitch.tv', opts.nick, opts);
  
  /*this.on('connect', function(){ this.readyState = 'open'; });
  this.on('abort', function(){ this.readyState = 'closed'; });*/
}
util.inherits(DefensIRC, Client);

DefensIRC.NotConnectedError = _.noop;

Object.defineProperty(DefensIRC.prototype, 'readyState', {
  get: function(){
    return this.conn? this.conn.readyState : 'closed';
  }
});

DefensIRC.prototype.close = function(){
  this.disconnect();
};
